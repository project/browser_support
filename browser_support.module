<?php

/**
 * @file
 * Display a custom site maintenence page for unsupported browsers.
 *
 * @dependencies
 * Browscap
 */
 
/**
 * Implementation of hook_menu()
 */
function browser_support_menu($may_cache) {
  $items = array();
  
	if ($may_cache) {
		$items[] = array(
      'path' => 'admin/settings/browser_support',
      'title' => t('Browser Support Settings'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('browser_support_settings_form'),
      'access' => user_access('administer site configuration')
    );
	}
  
  return $items;
}

/**
 * Implemenation of hook_settings()
 *
 */
function browser_support_settings_form() {
  // Check dependencies
  if (!variable_get('browscap_version', 0)) {
    drupal_set_message(t('You must first populate the browscap table by running %cron to use this module.', array('%cron' => l(t('cron'), 'cron.php'))), 'error');
    return;
  }

  // List all distinct browsers from the browscap table to create a select list of unsupported browsers
  $options = array();
  $query = db_query('SELECT * FROM {browscap}');
  while ($browser = db_fetch_object($query)) {
    $browser = unserialize($browser->data);
    if ($browser['parent']) {
    	$options[$browser['parent']] = $browser['parent'];
    }
  }
  $form['browser_support_unsupported'] = array(
    '#type' => 'select',
    '#title' => t('List of unsupported browsers'),
    '#default_value' => variable_get('browser_support_unsupported', array()),
    '#options' => $options,
    '#description' => t('Select the browsers that are not supported.'),
    '#multiple' => TRUE
  );

  // Give a message to display on the unsupported page.
  $form['browser_support_unsupported_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message to display on unsupported browsers'),
    '#default_value' => variable_get('browser_support_unsupported_message', ''),
    '#rows' => 5
  );

  $form['browser_support_semisupported'] = array(
    '#type' => 'select',
    '#title' => t('List of semi-supported browsers'),
    '#options' => $options,
    '#default_value' => variable_get('browser_support_semisupported', array()),
    '#description' => t('Select browsers that are semi-supported. Pages will load with a warning.'),
    '#multiple' => TRUE
  );

  $form['browser_support_semisupported_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message to display on semi-supported browsers'),
    '#default_value' => variable_get('browser_support_semisupported_message', ''),
    '#rows' => 3
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  
  $formats = filter_formats();

  foreach($formats AS $key => $format) {
  	$format_options[$key] = $format->name;
  }
  
  $form['advanced']['browser_support_unsupported_message_format'] = array(
    '#type' => 'radios',
    '#title' => t('Unsupported message format'),
    '#default_value' => variable_get('browser_support_unsupported_message_format', FILTER_FORMAT_DEFAULT),
    '#options' => $format_options
  );
  
  $form['advanced']['browser_support_semisupported_message_format'] = array(
    '#type' => 'radios',
    '#title' => t('Semi-supported message format'),
    '#default_value' => variable_get('browser_support_unsupported_message_format', FILTER_FORMAT_DEFAULT),
    '#options' => $format_options,
    '#description' => t('This is only used if the warning type is "drupal", otherwise all non-text elements will be stripped out.')
  );

  $form['advanced']['browser_support_allow_view'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow unsupported browsers to view site'),
    '#default_value' => variable_get('browser_support_allow_view', 0)
  );
  
  $form['advanced']['browser_support_allow_view_message'] = array(
    '#type' => 'textfield',
    '#title' => t('View Site Text'),
    '#description' => t('Text for the link that will allow the user to proceed to the site anyways.'),
    '#default_value' => variable_get('browser_support_allow_view_message', '')
  );

  $warning_types = array('drupal' => t('Drupal Message'), 'javascript' => t('Javascript alert'));

  $form['advanced']['browser_support_warning_type'] = array(
    '#type' => 'radios',
    '#title' => t('Warning Type'),
    '#description' => t('The type of warning users will receive when viewing with a semi-supported browser'),
    '#default_value' => variable_get('browser_support_warning_type', 'drupal'),
    '#options' => $warning_types,
    '#required' => TRUE
  );

  $form['advanced']['browser_support_warning_session'] = array(
    '#type' => 'checkbox',
    '#title' => t('One warning per session'),
    '#default_value' => variable_get('browser_support_warning_session', 0),
    '#description' => t('Only display the message once per session.')
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_init()
 */
function browser_support_init() {
  // Check settings and allow admins to view site regardless of browser
  // If set, allow users with forceview set in $_GET to view the site as well
  if ($_GET['forceview'] == 'yes') {
  	$_SESSION['browser_support_force_view'] = TRUE;
  }
  
  if (!variable_get('browscap_version', 0) || user_access('access administration pages') || (variable_get('browser_support_allow_view', 0) && $_SESSION['browser_support_force_view'])) {
    return;
  }

  // Get the current browser and the unsupported browser list.
  $unsupported = variable_get('browser_support_unsupported', array());
  $browser = browscap_get_browser();
  
  // If it is unsupported print the site maintenence page with custom content
  if (in_array($browser['parent'], $unsupported) && !(variable_get('browser_support_warning_session', 0) && $_SESSION['browser_support_warned'])) {
    drupal_set_header('HTTP/1.0 503 Service unavailable');
    drupal_set_title(t(variable_get('browser_support_unsupported_title', 'Unsupported Browser')));
    
    $content = check_markup(variable_get('browser_support_unsupported_message', ''), variable_get('browser_support_unsupported_message_format', FILTER_FORMAT_DEFAULT), FALSE);
    
    if (variable_get('browser_support_allow_view', 0)) {
    	$content .= '<p>'. l(t(variable_get('browser_support_allow_view_message', 'View Site')), $_GET['q'], array(), 'forceview=yes');
    }
    
    $_SESSION['browser_support_warned'] = TRUE;
    print theme('maintenance_page', $content, FALSE);
    exit;
  }

  $semisupported = variable_get('browser_support_semisupported', array());

  // If the browser is only semi-supported, display a warning
  if (in_array($browser['parent'], $semisupported)) {
      if (!(variable_get('browser_support_warning_session', 0) && $_SESSION['browser_support_warned'])) {
          switch(variable_get('browser_support_warning_type', 'drupal')) {
             case 'drupal':
                drupal_set_message(check_markup(t(variable_get('browser_support_semisupported_message', '')), variable_get('browser_support_semisupported_message_format', FILTER_FORMAT_DEFAULT), FALSE), 'error');
             break;
             case 'javascript':
                drupal_set_html_head("<script type='text/javascript'>\n<!--\nalert(\"". addslashes(t(strip_tags(variable_get('browser_support_semisupported_message', '')))) ."\");\n-->\n</script>");
             break;
          }
          $_SESSION['browser_support_warned'] = TRUE;
      }
  }
}
