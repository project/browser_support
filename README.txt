********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Browser Support Module
Author: Sarva Bryant <sarva at aliandesign dot com>
Drupal: 5.0.x
********************************************************************
DESCRIPTION:

This module allows you to specify a list of browsers that your
website does not support or are only semi-supported.

Unsupported browsers will be shown the sites maintenence page
with a custom message while semi-supported browsers will
get a javascript alert or a drupal message.

You can allow users to forcefully view the site even if they
are using an unsupported browser.
********************************************************************
INSTALLATION:

1. Put the browser_support module into your modules directory.

2. Enable the browser_support module by navigating to:

     Administer->Build->Modules

3. Go to Administer->Settings->browser_support and configure
   the settings.

********************************************************************
NOTES:

This module is dependant on the browscap module being installed
and configured properly beforehand.

********************************************************************

TODO:

